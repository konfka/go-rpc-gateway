PHONY: generate-structs
generate-structs:
	mkdir -p rpc/proto_user
	mkdir -p rpc/proto_auth
	mkdir -p rpc/proto_worker

	protoc --proto_path=rpc --go_out=rpc/proto_worker --go_opt=paths=source_relative rpc/proto_worker.proto
	protoc --proto_path=rpc --go_out=rpc/proto_user --go_opt=paths=source_relative rpc/proto_user.proto
	protoc --proto_path=rpc --go_out=rpc/proto_auth --go_opt=paths=source_relative rpc/proto_auth.proto