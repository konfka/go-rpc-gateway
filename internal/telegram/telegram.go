package telegram

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/konfka/go-rpc-gateway/config"
	"go.uber.org/zap"
	"sync"
)

type Telegram struct {
	conf   config.Telegram
	logger *zap.Logger
	bot    *tgbotapi.BotAPI
	sync.RWMutex
}

func NewTelegramBot(conf config.Telegram, logger *zap.Logger) *Telegram {
	bot, err := tgbotapi.NewBotAPI(conf.Token)
	if err != nil {
		logger.Fatal(fmt.Sprintf("telegram not connect err:,%s", err))
	}
	logger.Info(fmt.Sprintf("telegram connect authorized: %s", bot.Self.UserName))
	return &Telegram{conf: conf, logger: logger, bot: bot}
}

func (t *Telegram) updateTelegram() int64 {
	//Устанавливаем время обновления
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 10

	updates := t.bot.GetUpdatesChan(u)
	for r := range updates {
		if r.Message == nil {
			continue
		}
		//получаем необходимый id
		id := r.Message.Chat.ID
		return id
	}
	return 0
}

func (t *Telegram) Run(ctx context.Context, logger *zap.Logger) error {

	return t.RabbitConnect(logger, ctx)

}

type Rabbit struct {
	logger      *zap.Logger
	amqpChannel *amqp091.Channel
}

func (t *Telegram) RabbitConnect(logger *zap.Logger, ctx context.Context) error {
	//Установка соединения с RabbitMQ
	conn, err := amqp091.Dial("amqp://guest:guest@rabbitwork:5672/") // localhost
	if err != nil {
		logger.Error("не удалось установить соединение с RabbitMq", zap.Error(err))
		return err
	}
	defer conn.Close()
	//Установление канала(протокол связи по соединению)
	amqpChannel, err := conn.Channel()
	if err != nil {
		logger.Error("не удалось установить соединение с каналом", zap.Error(err))
		return err
	}
	defer amqpChannel.Close()

	//сообщение серверу об интересующей нас задаче
	//Add - название очереди
	queue, err := amqpChannel.QueueDeclare("add", true, false, false, false, nil)
	if err != nil {
		logger.Error("не удалось отправить задачу 'add'' ", zap.Error(err))

	}
	//Параметры чтобы очереди выполнялись последовательно
	err = amqpChannel.Qos(1, 0, false)
	if err != nil {
		logger.Error("ошибка установления очередей ", zap.Error(err))

	}
	messageChannel, err := amqpChannel.Consume(queue.Name, "", true, false, false, false, nil)
	//Канал чтобы горутина работала в фоновом режиме
	stopChan := make(chan bool)
	//получаем id от бота
	id := t.updateTelegram()

	go func() {
		for message := range messageChannel {
			//t.RWMutex.Lock()
			logger.Info(fmt.Sprintf("recieve a body: %s", message.Body))

			//получаем сообщение от Rabbit и отправляем в бота
			_, err = t.bot.Send(tgbotapi.NewMessage(id, string(message.Body)))
			if err != nil {
				logger.Error("ошибка отправки сообщения:", zap.Error(err))
			}
			//t.RWMutex.Unlock()
		}
	}()
	select {
	case <-stopChan:
		logger.Info("закрытие канала")
		return err
	case <-ctx.Done():
	}
	return nil
}
