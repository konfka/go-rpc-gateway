package modules

import (
	"gitlab.com/konfka/go-rpc-gateway/internal/infrastructure/component"
	acontroller "gitlab.com/konfka/go-rpc-gateway/internal/modules/auth/controller"
	ucontroller "gitlab.com/konfka/go-rpc-gateway/internal/modules/user/controller"
	wcontroller "gitlab.com/konfka/go-rpc-gateway/internal/modules/worker/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
	Work wcontroller.Workerer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	workerController := wcontroller.NewWorker(services.Work, components)

	return &Controllers{
		Auth: authController,
		User: userController,
		Work: workerController,
	}
}
