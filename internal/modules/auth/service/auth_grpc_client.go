package service

import (
	"context"
	"gitlab.com/konfka/go-rpc-gateway/internal/infrastructure/errors"
	"gitlab.com/konfka/go-rpc-gateway/rpc/proto_auth"
)

type ExchangeServiceGRPCClient struct {
	client proto_auth.AuthServiceRPCClient
}

func (a *ExchangeServiceGRPCClient) Register(ctx context.Context, in RegisterIn) RegisterOut {
	req, err := a.client.Register(ctx, &proto_auth.RegisterIn{
		Email:    in.Email,
		Password: in.Password,
		//IdempotencyKey: string(rune(field)),
	})
	if err != nil {
		return RegisterOut{
			ErrorCode: errors.AuthServiceGeneralErr,
		}
	}
	return RegisterOut{
		Status:    int(req.GetStatus()),
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (a *ExchangeServiceGRPCClient) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	req, err := a.client.AuthorizeEmail(ctx, &proto_auth.AuthorizeEmailIn{
		Email:          in.Email,
		Password:       in.Password,
		RetypePassword: in.RetypePassword,
	})
	if err != nil {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	return AuthorizeOut{
		UserID:       int(req.GetUserId()),
		AccessToken:  req.GetAccessToken(),
		RefreshToken: req.GetRefreshToken(),
		ErrorCode:    int(req.GetErrorCode()),
	}
}

func (a *ExchangeServiceGRPCClient) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	req, err := a.client.AuthorizeRefresh(ctx, &proto_auth.AuthorizeRefreshIn{
		UserId: int32(in.UserID),
	})
	if err != nil {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceRefreshTokenGenerationErr,
		}
	}
	return AuthorizeOut{
		UserID:       int(req.GetUserId()),
		AccessToken:  req.GetAccessToken(),
		RefreshToken: req.GetRefreshToken(),
		ErrorCode:    int(req.GetErrorCode()),
	}
}

func (a *ExchangeServiceGRPCClient) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	req, err := a.client.AuthorizePhone(ctx, &proto_auth.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int32(in.Code),
	})
	if err != nil {
		return AuthorizeOut{
			ErrorCode: errors.UserServiceWrongPhoneCodeErr,
		}
	}
	return AuthorizeOut{
		UserID:       int(req.GetUserId()),
		AccessToken:  req.GetAccessToken(),
		RefreshToken: req.GetRefreshToken(),
		ErrorCode:    int(req.GetErrorCode()),
	}
}

func (a *ExchangeServiceGRPCClient) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	req, err := a.client.SendPhoneCode(ctx, &proto_auth.SendPhoneCodeIn{
		Phone: in.Phone,
	})
	if err != nil {
		return SendPhoneCodeOut{
			Code: errors.UserServiceWrongPhoneCodeErr,
		}
	}
	return SendPhoneCodeOut{
		Phone: req.GetPhone(),
		Code:  int(req.GetCode()),
	}
}

func (a *ExchangeServiceGRPCClient) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	req, err := a.client.VerifyEmail(ctx, &proto_auth.VerifyEmailIn{
		Hash:  in.Hash,
		Email: in.Email,
	})
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	return VerifyEmailOut{
		Success:   req.GetSuccess(),
		ErrorCode: int(req.GetErrorCode()),
	}
}

func NewExchangeServiceGRPC(client proto_auth.AuthServiceRPCClient) *ExchangeServiceGRPCClient {
	return &ExchangeServiceGRPCClient{client: client}
}
