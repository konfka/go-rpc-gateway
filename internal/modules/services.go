package modules

import (
	client2 "gitlab.com/konfka/go-rpc-gateway/internal/infrastructure/client"
	"gitlab.com/konfka/go-rpc-gateway/internal/infrastructure/component"
	aservice "gitlab.com/konfka/go-rpc-gateway/internal/modules/auth/service"
	uservice "gitlab.com/konfka/go-rpc-gateway/internal/modules/user/service"
	wservice "gitlab.com/konfka/go-rpc-gateway/internal/modules/worker/service"
	"gitlab.com/konfka/go-rpc-gateway/rpc/proto_auth"
	"gitlab.com/konfka/go-rpc-gateway/rpc/proto_user"
	"gitlab.com/konfka/go-rpc-gateway/rpc/proto_worker"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
	Work wservice.Workerer
}

func NewServicesJRPC(components *component.Components) *Services {
	var authClient client2.Client
	var userClient client2.Client
	var workerClient client2.Client

	authClient = client2.NewJSONRPC(components.Conf.AuthRPC, components.Logger)
	userClient = client2.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	workerClient = client2.NewJSONRPC(components.Conf.WorkerRPC, components.Logger)

	authClientRPC := aservice.NewAuthServiceJSONRPC(authClient)
	userClientRPC := uservice.NewUserServiceJSONRPC(userClient)
	workerClientRPC := wservice.NewWorkServiceJsonRPC(workerClient)

	return &Services{
		User: userClientRPC,
		Auth: authClientRPC,
		Work: workerClientRPC,
	}
}

func NewServicesGRPC(components *component.Components) *Services {
	authGRPS := client2.NewJSONGRPC(components.Conf.AuthRPC, components.Logger)
	authClient := proto_auth.NewAuthServiceRPCClient(authGRPS)
	authService := aservice.NewExchangeServiceGRPC(authClient)

	workerGRPS := client2.NewJSONGRPC(components.Conf.WorkerRPC, components.Logger)
	workerClient := proto_worker.NewExchangeServiceRPCClient(workerGRPS)
	workerService := wservice.NewExchangeServiceGRPC(workerClient)

	userGRPS := client2.NewJSONGRPC(components.Conf.UserRPC, components.Logger)
	userClient := proto_user.NewUserServiceRPCClient(userGRPS)
	userService := uservice.NewExchangeServiceGRPC(userClient)

	return &Services{
		Work: workerService,
		Auth: authService,
		User: userService,
	}

}
